database:
  host: '{{.Teller.EnvByKey "WP_DB_URL" "" }}'
  port: {{.Teller.EnvByKey "WP_DB_PORT" "" }}
  optional: '{{.Teller.EnvByKey "WP_OPTION" "missing" }}'